import Axios from 'axios';
import config from '../config';

Axios.defaults.baseURL = config.base_uri;

export default {
  daftar: [],
  getDaftar() {
    return new Promise((resolve) => {
      Axios.get('/skp')
        .then((result) => {
          this.daftar = result;
          return resolve(result);
        })
        .catch((err) => {
          console.error('whoops there\'s an error ');
          console.error(err);
          console.info('returning previous data');

          return resolve(this.daftar);
        });
    });
  },
  tambahSkp(data) {
    return new Promise((resolve, reject) => {
      Axios.post('/skp', data).then(result => resolve(result.data)).catch(err => reject(err.response.data));
    });
  },
};
