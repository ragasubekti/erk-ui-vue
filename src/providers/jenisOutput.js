import axios from 'axios';
import config from '../config';

axios.defaults.baseURL = config.base_uri;

export default {
  daftar: [],
  getDaftar() {
    return new Promise((resolve) => {
      axios.get('/jenis-output')
        .then((result) => {
          this.daftar = result.data.data;
          return resolve(this.daftar);
        })
        .catch((err) => {
          console.error('oops... i think there\'s something wrong');
          console.error(err);
          console.info('returning previous data');
          return resolve(this.daftar);
        });
    });
  },
};
