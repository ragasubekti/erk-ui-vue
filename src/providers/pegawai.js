import axios from 'axios';
import config from '../config';

axios.defaults.baseURL = config.base_uri;
axios.defaults.headers = {
  Authorization: `Bearer ${localStorage.getItem('bearerToken')}`,
};

const definition = {
  daftar: [],
  detail: {},
  detailDaftar: {},
  getListPegawai() {
    return new Promise((resolve) => {
      axios.get('/pegawai').then((res) => {
        this.daftar = res.data.data;
        return resolve(this.daftar);
      }).catch((err) => {
        console.warn('HEADS UP! ERROR AHEAD!');
        console.error(err);
        console.info('Returning Previous Data');
        return resolve(this.daftar);
      });
    });
  },
  getDetailPegawai(id) {
    return new Promise((resolve) => {
      axios.get(`/pegawai/${id}`).then((res) => {
        this.detailDaftar = res.data.data;
        return resolve(this.detailDaftar);
      }).catch((err) => {
        console.warn('HEADS UP! ERROR AHEAD!');
        console.error(err);
        console.info('Returning Previous Data');
        return resolve(this.detailDaftar);
      });
    });
  },
  getProfilPegawai() {
    return new Promise((resolve) => {
      axios.get('/profile').then((res) => {
        this.detail = res.data.data;
        return resolve(this.detail);
      }).catch((err) => {
        console.error('Something bad has happened i think...');
        console.error(err);
        console.info('Anyway i returned the previous data so...');
        return resolve(this.detail);
      });
    });
  },
  // detail: {}
};

export default definition;
