import axios from 'axios';
import router from '../router';
import config from '../config';


axios.defaults.baseURL = config.base_uri;
axios.defaults.headers = {
  Authorization: `Bearer ${localStorage.getItem('bearerToken')}`,
};

export default {
  user: {
    authenticated: !!localStorage.getItem('bearerToken'),
    // authenticated: true,
  },
  login(username, password) {
    return new Promise((resolve, reject) => {
      axios
        .post('/authentication', {
          username,
          password,
        })
        .then(result => resolve(result.data))
        .catch(err => reject(err));
    });
  },
  check: () => new Promise((resolve, reject) => {
    // return resolve(true);
    axios.get('/auth/verify').then(result => resolve(result.data)).catch((err) => {
      localStorage.removeItem('bearerToken');
      return reject(err);
    });
  }),
  auth(username, password) {
    return new Promise((resolve, reject) => {
      this.login(username, password).then((result) => {
        localStorage.setItem('bearerToken', result.data.token);
        axios.defaults.headers = {
          Authorization: `Bearer ${localStorage.getItem('bearerToken')}`,
        };
        router.go('/');
        return resolve({ success: true });
      }).catch((err) => {
        if (err.response) return reject(err.response.data);
        return reject(new Error('Cannot connect to server, try again'));
      });
    });
  },
  logout: () => {
    // localStorage.removeItem('bearerToken');
    // this.user.aut;
  },
};
