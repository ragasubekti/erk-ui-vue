import axios from 'axios';
import config from '../config';

axios.defaults.baseURL = config.base_uri;

export default {
  daftarTarget: [],
  getDaftarTarget() {},
  tambahTarget(data) {
    return new Promise((resolve, reject) => {
      axios.post('/target-skp', data)
        .then(result => resolve(result.data))
        .catch(err => reject(err.response.data));
    });
  },
};
