import axios from 'axios';
import config from '../config';

axios.defaults.baseURL = config.base_uri;

export default {
  daftar: [],
  getDaftar() {
    return new Promise((resolve) => {
      axios.get('/satuan-waktu')
        .then((result) => {
          this.daftar = result.data.data;
          return resolve(this.daftar);
        })
        .catch((err) => {
          console.error('welp something nasty has happened');
          console.error(err);
          console.info('using previous data');
          return resolve(this.daftar);
        });
    });
  },
};
