import Vue from 'vue';
import Router from 'vue-router';
import Aktifitas from './views/Aktifitas.vue';
import CapaianSkp from './views/CapaianSkp.vue';
import Dashboard from './views/Dashboard.vue';
import Login from './views/Login.vue';
import ePresensi from './views/ePresensi.vue';
import MonitoringPegawai from './views/MonitoringPegawai.vue';
import ReviewAktifitas from './views/ReviewAktifitas.vue';
import Skp from './views/Skp.vue';
import TargetSkp from './views/TargetSkp.vue';

import auth from './providers/auth';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: (to, from, next) => {
        if (auth.user.authenticated) {
          auth.check().then(() => next()).catch(() => next('/login'));
        } else {
          next('/login');
        }
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: (to, from, next) => {
        if (auth.user.authenticated) {
          auth.check().then(() => next('/')).catch(() => next());
        } else {
          next();
        }
      },
    },
    {
      path: '/aktifitas',
      name: 'aktifitas',
      component: Aktifitas,
        beforeEnter: (to, from, next) => {
            if (auth.user.authenticated) {
                auth.check().then(() => next()).catch(() => next('/login'));
            } else {
                next('/login');
            }
    },
    },
    {
      path: '/capaian-skp',
      name: 'capaian-skp',
      component: CapaianSkp,
        beforeEnter: (to, from, next) => {
            if (auth.user.authenticated) {
                auth.check().then(() => next()).catch(() => next('/login'));
            } else {
                next('/login');
            }
        },
    },
    {
      path: '/e-presensi',
      name: 'e-presensi',
      component: ePresensi,
        beforeEnter: (to, from, next) => {
            if (auth.user.authenticated) {
                auth.check().then(() => next()).catch(() => next('/login'));
            } else {
                next('/login');
            }
        },
    },
    {
      path: '/monitoring-pegawai',
      name: 'monitoring-pegawai',
      component: MonitoringPegawai,
        beforeEnter: (to, from, next) => {
            if (auth.user.authenticated) {
                auth.check().then(() => next()).catch(() => next('/login'));
            } else {
                next('/login');
            }
        },

    },
    {
      path: '/review-aktifitas',
      name: 'review-aktifitas',
      component: ReviewAktifitas,
        beforeEnter: (to, from, next) => {
            if (auth.user.authenticated) {
                auth.check().then(() => next()).catch(() => next('/login'));
            } else {
                next('/login');
            }
        },

    },
    {
      path: '/skp',
      name: 'skp',
      component: Skp,
        beforeEnter: (to, from, next) => {
            if (auth.user.authenticated) {
                auth.check().then(() => next()).catch(() => next('/login'));
            } else {
                next('/login');
            }
        },

    },
    {
      path: '/target-skp',
      name: 'target-skp',
      component: TargetSkp,
        beforeEnter: (to, from, next) => {
            if (auth.user.authenticated) {
                auth.check().then(() => next()).catch(() => next('/login'));
            } else {
                next('/login');
            }
        },

    },
  ],
});
