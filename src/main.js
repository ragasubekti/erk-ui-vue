import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'uikit/dist/css/uikit-core.min.css';
import 'element-ui/lib/theme-chalk/index.css';

import Vue from 'vue';
import Vuikit from 'vuikit';
import VuikitIcons from '@vuikit/icons';
import BootstrapVue from 'bootstrap-vue';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/id';

import App from './App.vue';
import router from './router';
import store from './store';

import './assets/bootstrap-extend.min.css';
import './assets/site.min.css';
import './assets/style.css';

import './registerServiceWorker';

// Vue.config.productionTip = false;
Vue.use(Vuikit);
Vue.use(VuikitIcons);
Vue.use(BootstrapVue);
Vue.use(ElementUI, { locale });

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
